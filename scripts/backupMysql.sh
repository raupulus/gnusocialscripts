#!/bin/bash

echo "A continuacion se realizara un backup de las tablas de las BBDD"
sleep "2"

sudo mysqldump --user=root --password=yourpass --all-databases > /etc/mysql/backups_mysql/safecopyBDsocial.sql -pyourpass

echo "Ahora se realizara un check de todas las BBDD y se enviara el resultado a los administradores"
sleep "2"

sudo mysqlcheck --check gnusocial --user="root" --password="yourpass" > /etc/mysql/backups_mysql/checkingBD.tmp -pyourpass
sudo mysqlcheck --check gnusocialbeta --user="root" --password="yourpass" > /etc/mysql/backups_mysql/checkingBD.tmp -pyourpass
sudo mysqlcheck --check information_schema --user="root" --password="yourpass" > /etc/mysql/backups_mysql/checkingBD.tmp -pyourpass
sudo mysqlcheck --check mysql --user="root" --password="yourpass" > /etc/mysql/backups_mysql/checkingBD.tmp -pyourpass
sudo mysqlcheck --check performance_schema --user="root" --password="yourpass" > /etc/mysql/backups_mysql/checkingBD.tmp -pyourpass
sudo mysqlcheck --check phpmyadmin --user="root" --password="yourpass" > /etc/mysql/backups_mysql/checkingBD.tmp -pyourpass
sudo mysqlcheck --check qgnusocial --user="root" --password="yourpass" > /etc/mysql/backups_mysql/checkingBD.tmp -pyourpass